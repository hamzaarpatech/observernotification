//
//  BaseVC.swift
//  ObserverNotification
//
//  Created by Hamza on 2/17/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
let bulbasurNotifierKey = "bulbasur.calling"
let pachirisuNotifierKey = "pachirisu.calling"

class BaseVC: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pokeballButton: UIButton!
    let pokemon1 = Notification.Name(rawValue: bulbasurNotifierKey)
    let pokemon2 = Notification.Name(rawValue: pachirisuNotifierKey)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createObservers()
    }
    
    @IBAction func pokeballTapped(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "SecondVC") as! SecondVC
        present(vc, animated: true, completion: nil)
    }
    
    func createObservers(){
    // Pokemon1
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateImage(notification:)), name: pokemon1, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateLabel(notification:)), name: pokemon1, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateBackground(notification:)), name: pokemon1, object: nil)
    // Pokemon2
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateImage(notification:)), name: pokemon2, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateLabel(notification:)), name: pokemon2, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.updateBackground(notification:)), name: pokemon2, object: nil)
    }
    
    @objc func updateImage(notification : NSNotification){
        let isPokemon1 = notification.name == pokemon1
        let updatedimage = isPokemon1 ? UIImage(named: "bulbasaur_caught") : UIImage(named: "pachirisu_caught")
        mainImageView.image = updatedimage
    }
    
    @objc func updateLabel(notification : NSNotification){
        let isPokemon1 = notification.name == pokemon1
        let updatedLabel = isPokemon1 ? "Bulbasaur Caught" : "Pachirisu Caught"
        nameLabel.text = updatedLabel
    }
    
    @objc func updateBackground(notification : NSNotification){
        let isPokemon1 = notification.name == pokemon1
        let updatedBg = isPokemon1 ? UIColor.darkGray : UIColor.gray
        view.backgroundColor = updatedBg
    }
}
