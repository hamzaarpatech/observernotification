//
//  SecondVC.swift
//  ObserverNotification
//
//  Created by Hamza on 2/17/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func bulbasaurTapped(_ sender: UIButton) {
        let name = Notification.Name(rawValue: bulbasurNotifierKey)
        NotificationCenter.default.post(name: name, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func PachirisuTapped(_ sender: UIButton) {
        let name = Notification.Name(rawValue: pachirisuNotifierKey)
        NotificationCenter.default.post(name: name, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
}
